#' @title Convert list.column data.frame into scalar.column data.frame
#'
#' @description Convert data.frame with 2 column.list into data.frame with only column.scalar
#'
#' @name fd_melt
#'
#' @param x A data.frame
#' @param key1 character string. The name of the column to select
#' @param key2 icharacter string. The name of the column to select
#' @param id name of the replicate for the id. As to be of the same length as the number of row of the x object
#'
#' @export
#'
fd_melt_DOUBLE <- function(x, key1, key2, id = NULL){
  lgth_key1 = sapply(x[[key1]], length)
  lgth_key2 = sapply(x[[key2]], length)
  if(any(lgth_key1 != lgth_key2)){ stop("length of element from 'key1' and 'key2' differ.")}
  if(is.null(id)) {
    id = 1:length(lgth_key1)
  } else{
    id = x[[id]]
  }
  df = data.frame(
    id = do.call("c", lapply(1:length(id), function(i) rep(id[i], lgth_key1[[i]])))
  )
  df[[key1]] = do.call("c", x[[key1]])
  df[[key2]] = do.call("c", x[[key2]])
  return(df)
}

#' @name fd_melt
#'
#'
#' @export
#'
fd_melt_SINGLE <- function(x, key1, id = NULL){
  lgth_key1 = sapply(x[[key1]], length)
  if(is.null(id)) {
    id = 1:length(lgth_key1)
  } else{
    id = x[[id]]
  }
  df = data.frame(
    id = do.call("c", lapply(1:length(id), function(i) rep(id[i], lgth_key1[[i]])))
  )
  df[[key1]] = do.call("c", x[[key1]])
  return(df)
}

#' @name fd_melt
#'
#' @param keep vector of column name to keep
#'
#' @export
#'
fd_melt_12 <- function(x, key1, key2 = NULL, id = NULL, keep = NULL){

  if(is.null(key2)){
    DF = fd_melt_SINGLE(x, key1, id)
  } else{
    DF = fd_melt_DOUBLE(x, key1, key2, id)
  }

  # --- keep
  if(!is.null(keep)) {
    for(i in 1:length(keep)){
      DF[[keep[[i]]]] = fd_melt_STICK(x, key1, keep[[i]])
    }
  }
  return(DF)
}


#' @name fd_melt
#'
#' @param key a list or vector of column name to keep
#'
#' @export
#'
fd_melt <- function(x, key, id = NULL, keep = NULL){
  lgth_key = lapply(1:length(key),
                    function(i){
                      sapply(x[[key[[i]]]], length)
                    })
  if(length(lgth_key)>1){
    if(!all(
      sapply(2:length(lgth_key),
             function(i) lgth_key[[1]] == lgth_key[[i]])
    )) { stop("length of element within 'key' differ.")}
  }

  if(is.null(id)) {
    id = 1:length(lgth_key[[1]])
  } else{
    id = x[[id]]
  }
  DF = data.frame(
    id = do.call("c", lapply(1:length(id), function(i) rep(id[i], lgth_key[[1]][[i]])))
  )
  for(i in 1:length(key)){
    DF[[key[[i]]]] = do.call("c", x[[key[[i]]]])
  }
  # --- keep
  if(!is.null(keep)) {
    for(i in 1:length(keep)){
      DF[[keep[[i]]]] = fd_melt_STICK(x, key[[1]], keep[[i]])
    }
  }
  return(DF)
}

#' @name fd_melt
#'
#' @export
#'
fd_melt_STICK = function(x, key1, keep){
  if(is.factor(x[[keep]])){
    vec = factor(do.call("c", lapply(1:nrow(x),
                                     function(i){ as.character(rep(x[[keep]][i], length(x[[key1]][[i]])))})),
                 levels = levels(x[[keep]]))
  } else{
    vec = do.call("c", lapply(1:nrow(x),
                              function(i){ rep(x[[keep]][i], length(x[[key1]][[i]]))}))
  }
  return(vec)
}

#' @title Combine list of data.frame by Rows
#'
#' @return Return a data.frame
#'
#' @param ls A list of data.frame
#' @param id id to provide to each data.frame. Must be the length of the list
#'
#' @export
#'
fd_rbindLStoDF <- function(ls, id = NULL){
  df <- do.call("rbind", ls)
  nrow_df <- sapply(ls, nrow)
  if(is.null(id)) { id = 1:length(nrow_df) }
  df[["id"]] = do.call("c", lapply(1:length(nrow_df), function(i) rep(id[i], nrow_df[[i]])))
  return(df)
}
