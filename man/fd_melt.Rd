% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/melt.R
\name{fd_melt}
\alias{fd_melt}
\alias{fd_melt_DOUBLE}
\alias{fd_melt_SINGLE}
\alias{fd_melt_12}
\alias{fd_melt_STICK}
\title{Convert list.column data.frame into scalar.column data.frame}
\usage{
fd_melt_DOUBLE(x, key1, key2, id = NULL)

fd_melt_SINGLE(x, key1, id = NULL)

fd_melt_12(x, key1, key2 = NULL, id = NULL, keep = NULL)

fd_melt(x, key, id = NULL, keep = NULL)

fd_melt_STICK(x, key1, keep)
}
\arguments{
\item{x}{A data.frame}

\item{key1}{character string. The name of the column to select}

\item{key2}{icharacter string. The name of the column to select}

\item{id}{name of the replicate for the id. As to be of the same length as the number of row of the x object}

\item{keep}{vector of column name to keep}

\item{key}{a list or vector of column name to keep}
}
\description{
Convert data.frame with 2 column.list into data.frame with only column.scalar
}
