# fdf: a set of tools to work with functional data (column-list) in data frame. 


## Build status for development version

[![pipeline status](https://gitlab.paca.inra.fr/biosp/fdf/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/biosp/fdf/commits/master)


### Clone project

```bash
git clone git@gitlab.paca.inra.fr:biosp/fdf.git
```

### Generate Rd file

```r
roxygen2::roxygenize('fdf', roclets=c('rd', 'namespace'))
```

To generate the reference manual:

```r
R CMD Rd2pdf .
```

or from the parent folder

```r
R CMD Rd2pdf fdf/
```

### Generate package

```bash
R CMD BUILD fdf --resave-data
```

### Install package for Debug

```bash
R CMD INSTALL --no-multiarch --with-keep.source fdf_x.x.tar.gz
```

### Test package

Package unit test is done with `testthat` package. The integration of C++ test unit requires the add of a structure done with `testthat::use_catch()` (see `?use_catch` for details)

### Check package

```bash
R CMD check --as-cran fdf_x.x.x.tar.gz
```
# Author

Virgile Baudrot

