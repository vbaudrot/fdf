---
title: "start"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{start}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


The package `fdf` allow to work with column-list. When for instance you have a list of time series that you want to include in a `data.frame`.

Since it works only with `data.frame` object, there is no new object, you can use very easily with any package like `dplyr` (and the tidyverse) and spatio-temporal object with package `sf` for instance.

```{r setup}
library(fdf)
```

# Add a new column-list with `fd_add`

```{r addData}
DF = data.frame( A = 1:10)

DFD = fd_add(DF, "B", function(i) rnorm(runif(1, 4,20)))
DFD
```

# Filter element in a column-list with `fd_filter`

```{r filter}
fd_filter(DFD, "B", 1)

fd_filter(DFD, "B", 2)

fd_filter(DFD, "B", 3)
```

# Find index of element in column-list matching with a key using `fd_match`

```{r}
DFD = fd_add(DFD, "C", function(i) sample(1:5, 5))

idMatch = fd_match(DFD, "C", 4)
idMatch

# to check this:
fd_filter(DFD, "C", idMatch)
```

# Melting column-list to a classical data.frame with `fd_melt`

```{r meltSimple}
fd_melt(DFD, "B")

fd_melt(DFD, "B", keep = "A")

fd_melt(DFD, "B", keep = list("A", "C"))
```

when two column list have the same length for each elements, we can melt in the same time

```{r meltDouble}
DFD = fd_add(DFD, "B2", function(i) 1:length(DFD$B[[i]]))

fd_melt(DFD, "B", "B2")

fd_melt(DFD, "B", "B2", keep = list("A", "C"))
```
